/******************************************************************************
 * @file            process.h
 *****************************************************************************/
#ifndef     _PROCESS_H
#define     _PROCESS_H

int preprocess_init (void);
void process_file (const char *ifile);

#endif      /* _PROCESS_H */
